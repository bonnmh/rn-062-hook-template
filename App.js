import React, {Component} from 'react';
import {PersistGate} from 'redux-persist/integration/react';

import 'react-native-gesture-handler';
import RootContainerScreen from './src/root/RootContainerScreen';

import {store, startSaga, persistor, Provider} from './src/store/storeProvider';

export default class App extends Component {
  render() {
    startSaga();
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <RootContainerScreen />
        </PersistGate>
      </Provider>
    );
  }
}
