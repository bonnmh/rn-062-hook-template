import {connect} from 'react-redux';
import FollowerDetailScreen from './FollowerDetailScreen';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FollowerDetailScreen);
