import React, {useEffect, useMemo} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  StatusBar,
  Platform,
  ActivityIndicator,
  Image,
} from 'react-native';
import {useIsDrawerOpen} from '@react-navigation/drawer';
import colors from '../../themes/Colors';
import IDs from '../IDs';
import {barStyle, fontSize} from '../../themes/const';
import {isIphoneX} from '../../themes/Application.Style';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {useDispatch, useSelector} from 'react-redux';
import {actions, selectors} from '../../store';
import {navigation} from '../../app-navigation/rootNavigation';

const ProfileScreen = () => {
  const profile = useSelector((state) => selectors.profile.getData(state));
  const fetching = useSelector((state) => selectors.profile.getFetching(state));
  const dispatch = useDispatch();
  const getProfile = () =>
    dispatch(actions.profile.getProfileRequest('bonnmh'));

  const goDetailScreen = () => {
    navigation.navigate(IDs.ProfileDetail, {});
  };
  const renderToolbar = () => {
    return (
      <View style={styles.toolbar}>
        <StatusBar
          hidden={false}
          backgroundColor={colors.primary}
          barStyle={barStyle.lightContent}
        />
        <TouchableOpacity
          style={styles.viewWrapIcLeft}
          onPress={() => {
            navigation.toggleDrawer();
            console.log('toggleDrawer', navigation);
          }}>
          <Ionicons name="ios-menu" color="white" size={20} />
        </TouchableOpacity>
        <View style={styles.viewWrapTitleToolbar}>
          <Text style={styles.titleToolbar}>Profile</Text>
        </View>
        <View style={styles.viewWrapIcRight} />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {/* {renderToolbar()} */}
      <SafeAreaView style={styles.wrap}>
        <StatusBar
          hidden={false}
          backgroundColor={colors.primary}
          barStyle={barStyle.lightContent}
        />
        {profile && (
          <Image style={styles.avatar} source={{uri: profile.avatar_url}} />
        )}
        <TouchableOpacity onPress={goDetailScreen} style={styles.btn}>
          <Text>go to follower</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={getProfile} style={styles.btn}>
          {fetching ? (
            <ActivityIndicator size="small" color="red" />
          ) : (
            <Text>get profile github</Text>
          )}
        </TouchableOpacity>
      </SafeAreaView>
    </View>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  wrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    width: 200,
    height: 30,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    borderRadius: 20,
  },
  toolbar: {
    flexDirection: 'row',
    width: '100%',
    height: Platform.OS === 'android' ? 48 : isIphoneX() ? 88 : 78,
    paddingTop: Platform.OS === 'android' ? 0 : isIphoneX() ? 40 : 30,
    backgroundColor: colors.red,
    alignItems: 'center',
  },
  viewWrapTitleToolbar: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleToolbar: {
    color: colors.white,
    fontSize: fontSize.large,
  },
  viewWrapIcLeft: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icLeft: {
    width: 23,
    height: 23,
    tintColor: colors.white,
  },
  viewWrapIcRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icRight: {
    width: 23,
    height: 23,
    tintColor: colors.white,
  },
  textRight: {
    color: colors.white,
    fontSize: fontSize.medium,
  },
  viewHorizontalLine: {
    backgroundColor: colors.grey,
    height: 0.5,
    alignSelf: 'stretch',
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
});
