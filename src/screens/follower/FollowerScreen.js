import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const FollowerScreen = () => {
  return (
    <View style={styles.wrap}>
      <Text>FollowerScreen</Text>
    </View>
  );
};

export default FollowerScreen;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
