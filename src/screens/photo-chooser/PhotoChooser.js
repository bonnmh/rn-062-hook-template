import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const PhotoChooser = () => {
  return (
    <View style={styles.wrap}>
      <Text>PhotoChooser</Text>
    </View>
  );
};

export default PhotoChooser;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
