import {connect} from 'react-redux';
import PhotoChooser from './PhotoChooser';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(PhotoChooser);
