import React, {useContext} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ViewBase,
  TextInput,
  FlatList,
  ScrollView,
  Dimensions,
} from 'react-native';
import {
  NewTestProvider,
  useNewPostState,
  useNewTestDispatch,
  setTest,
} from './context/useTestContext';
import ipLocation from 'iplocation';
import {FetchHelper} from '../../utils';
import {eventChannel} from 'redux-saga';
const win = Dimensions.get('window');
const data = [
  {id: 1, content: '1'},
  {id: 2, content: '2'},
  {id: 3, content: '3'},
  {id: 4, content: '4'},
  {id: 5, content: '5'},
  {id: 6, content: '6'},
  {id: 7, content: '7'},
  {id: 8, content: '8'},
  {id: 9, content: '9'},
  {id: 10, content: '10'},
  {id: 11, content: '11'},
  {id: 12, content: '12'},
  {id: 13, content: '13'},
  {id: 14, content: '14'},
  {id: 15, content: '15'},
  {id: 16, content: '16'},
  {id: 17, content: '17'},
  {id: 18, content: '18'},
  {id: 19, content: '19'},
  {id: 20, content: '20'},
  {id: 21, content: '21'},
  {id: 22, content: '22'},
  {id: 23, content: '23'},
  {id: 24, content: '24'},
  {id: 25, content: '25'},
  {id: 26, content: '26'},
  {id: 27, content: '27'},
  {id: 28, content: '28'},
  {id: 29, content: '29'},
  {id: 30, content: '30'},
  {id: 31, content: '31'},
  {id: 32, content: '32'},
  {id: 33, content: '33'},
  {id: 34, content: '34'},
  {id: 35, content: '35'},
  {id: 226, content: '36'},
  {id: 227, content: '37'},
  {id: 228, content: '38'},
  {id: 229, content: '39'},
  {id: 3220, content: '40'},
  {id: 321, content: '41'},
  {id: 322, content: '42'},
  {id: 323, content: '43'},
  {id: 324, content: '44'},
  {id: 325, content: '45'},
];
const WatchScreen = () => {
  const dispatch = useNewTestDispatch();

  const {testData} = useNewPostState();
  const onPress = () => {
    alert(JSON.stringify(testData));
    dispatch(setTest({name: 'Nguyễn Minh Hiếu Bốn'}));
  };
  const [flag, setFlag] = React.useState(true);
  const [position, setPosition] = React.useState(0);
  const scrollEl = React.useRef();
  // let data;
  // React.useEffect(() => {
  //   let url = 'https://ipinfo.io/account/search?query=118.69.161.25';
  //   fetch(url)
  //     .then((response) => response.json())
  //     .then((responseJson) => {
  //       console.log(responseJson);
  //     })
  //     .catch((error) => {
  //       console.error(error);
  //     });
  // }, []);
  // console.log('data', data);
  const [term, setTerm] = React.useState(false);
  const renderItem = (item) => {
    // alert(JSON.stringify(item));
    return (
      <View
        key={item.id}
        style={{
          width: '100%',
          height: 500,
          backgroundColor: 'red',
          marginTop: 10,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text style={{fontSize: 30, color: 'white'}}>{item.item.content}</Text>
      </View>
    );
  };
  const onPressTest = async (e, i) => {
    setTerm(true);
    const wait = new Promise((resolve) => setTimeout(resolve, 10));
    wait.then(() => {
      if (scrollEl.current) {
        // scrollEl.current.scrollToOffset({
        //   animated: false,
        //   offset: i * 510,
        // });
        // scrollEl.current.scrollToItem({
        //   animated: false,
        //   item: e,
        // });
        scrollEl.current.scrollToIndex({
          animated: false,
          index: i,
        });
      }
    });

    setTerm(false);
    setFlag(!flag);
    // wait.then(() => {
    //   setFlag(!flag);
    // });
  };
  const handleScroll = (event) => {
    setPosition(event.nativeEvent.contentOffset.y);
  };
  if (term) {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: 'pink',
          position: 'absolute',
        }}></View>
    );
  }
  return (
    <>
      {term ? (
        <View
          style={{
            width: win.width,
            height: win.height,
            backgroundColor: 'pink',
            position: 'absolute',
          }}></View>
      ) : null}
      <View style={styles.wrap}>
        {/* <TouchableOpacity onPress={onPress}>
        <Text>ok</Text>
      </TouchableOpacity> */}

        <TouchableOpacity
          onPress={() => {
            setFlag(!flag);
          }}>
          <Text style={{fontSize: 35}}>TOGGLE</Text>
        </TouchableOpacity>
        {flag ? (
          <View
            style={{
              flex: 1,
              backgroundColor: 'pink',
              alignItems: 'center',
              // justifyContent: 'center',
            }}>
            <ScrollView>
              <View
                style={{
                  width: '100%',
                  flexWrap: 'wrap',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                }}>
                {data.map((e, i) => {
                  return (
                    <TouchableOpacity
                      key={i}
                      onPress={() => onPressTest(e, i)}
                      style={{
                        backgroundColor: 'white',
                        width: '30%',
                        height: 100,
                        marginTop: 10,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text>{e.content}</Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </ScrollView>
          </View>
        ) : (
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              // alignItems: 'center',
              justifyContent: 'center',
            }}>
            <FlatList
              ref={scrollEl}
              renderItem={(item) => renderItem(item)}
              keyExtractor={(item) => item.id}
              data={data}
              initialNumToRender={100}
              scrollEventThrottle={16}
              onScrollToIndexFailed={(error) => {
                // console.log(error);
                scrollEl.current.scrollToOffset({
                  offset: error.averageItemLength * error.index,
                  animated: false,
                });
                setTimeout(() => {
                  if (scrollEl.current) {
                    scrollEl.current.scrollToIndex({
                      index: error.index,
                      animated: false,
                    });
                  }
                }, 0);
              }}
              // onScroll={handleScroll}
            />
          </View>
        )}
      </View>
    </>
  );
};

export default WatchScreen;

const styles = StyleSheet.create({
  wrap: {flex: 1, backgroundColor: '#fff', zIndex: -1},
});
