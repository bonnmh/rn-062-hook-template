import React from 'react';
import PropTypes from 'prop-types';
const ActionTypes = {
  SET_TEST_DATA: 'SET_TEST_DATA',
};

const NewTestStateContext = React.createContext();
const NewTestDispatchContext = React.createContext();
export const setTest = (data) => ({
  type: ActionTypes.SET_TEST_DATA,
  payload: data,
});

const initialState = {
  testData: false,
};
function reducer(state, action) {
  switch (action.type) {
    case ActionTypes.SET_TEST_DATA: {
      return {...state, testData: action.payload};
    }
    default:
      return initialState;
  }
}

function NewTestProvider({children}) {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  return (
    <NewTestStateContext.Provider value={state}>
      <NewTestDispatchContext.Provider value={dispatch}>
        {children}
      </NewTestDispatchContext.Provider>
    </NewTestStateContext.Provider>
  );
}

NewTestProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

function useNewPostState() {
  const context = React.useContext(NewTestStateContext);
  if (context === undefined) {
    throw new Error('useNewPostState must be used within a NewPostProvider');
  }
  return context;
}

function useNewTestDispatch() {
  const context = React.useContext(NewTestDispatchContext);
  if (context === undefined) {
    throw new Error('useNewTestDispatch must be used within a NewPostProvider');
  }
  return context;
}

export {NewTestProvider, useNewPostState, useNewTestDispatch};
