import {connect} from 'react-redux';
import WatchScreen from './WatchScreen';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(WatchScreen);
