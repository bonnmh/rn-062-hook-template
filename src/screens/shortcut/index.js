import {connect} from 'react-redux';
import ShortCutScreen from './ShortCutScreen';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ShortCutScreen);
