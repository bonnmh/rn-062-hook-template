import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const LiveStream = () => {
  return (
    <View style={styles.wrap}>
      <Text>LiveStream</Text>
    </View>
  );
};

export default LiveStream;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
