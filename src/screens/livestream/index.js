import {connect} from 'react-redux';
import LiveStream from './LiveStream';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(LiveStream);
