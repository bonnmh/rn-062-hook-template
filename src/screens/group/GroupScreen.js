import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Stories from '../stories/Stories';
import ImageLayout from 'react-native-image-layout';
import {navigation} from '../../app-navigation/rootNavigation';
const GroupScreen = () => {
  return (
    <View style={styles.wrap}>
      <TouchableOpacity
        onPress={() => navigation.navigate('GroupAnimation')}
        style={{
          width: '100%',
          height: 50,
          backgroundColor: '#BF4650',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text style={{color: '#fff'}}>GroupAnimation</Text>
      </TouchableOpacity>
      {/* <Stories /> */}
      {/* <ScrollView>
        <View
          style={{
            flex: 1,
            backgroundColor: 'pink',
            flexWrap: 'wrap',
            flexDirection: 'row',
            justifyContent: 'space-around',
          }}>
          <View
            style={{
              width: '49%',
              height: 100,
              marginTop: 10,
              backgroundColor: 'white',
            }}
          />
        </View>
      </ScrollView> */}

      <ImageLayout
        columns={3}
        images={[
          {
            uri:
              'https://luehangs.site/pic-chat-app-images/animals-avian-beach-760984.jpg',
          },
          {
            // Version *2.0.0 update (or greater versions):
            // Does not require an id for each image
            // object, but is for good practice and
            // can be better for performance for API.
            id: 'blpccx4cn',
            uri:
              'https://luehangs.site/pic-chat-app-images/beautiful-blond-blonde-hair-478544.jpg',
          },
          {
            uri:
              'https://luehangs.site/pic-chat-app-images/attractive-balance-beautiful-186263.jpg',
          },
          {
            uri:
              'https://luehangs.site/pic-chat-app-images/beautiful-beautiful-women-beauty-40901.jpg',
          },
          {
            uri:
              'https://luehangs.site/pic-chat-app-images/beautiful-blond-fishnet-stockings-48134.jpg',
          },
          {
            uri:
              'https://luehangs.site/pic-chat-app-images/beautiful-beautiful-woman-beauty-9763.jpg',
          },
        ]}
      />
    </View>
  );
};

export default GroupScreen;

const styles = StyleSheet.create({
  wrap: {flex: 1, backgroundColor: '#EAEAEA'},
});
