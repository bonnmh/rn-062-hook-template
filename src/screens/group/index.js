import {connect} from 'react-redux';
import GroupScreen from './GroupScreen';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(GroupScreen);
