import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  KeyboardAvoidingView,
  SafeAreaView,
} from 'react-native';
import {connect} from 'react-redux';
import Metrics from '../../themes/Metrics';

export class FullPostTool extends Component {
  render() {
    return (
      <KeyboardAvoidingView
        style={styles.parentContainer}
        enabled
        behavior="height">
        <SafeAreaView style={styles.container}>
          <Text> FullPostTool </Text>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(FullPostTool);

const styles = StyleSheet.create({
  parentContainer: {
    height: Metrics.appHeight,
    position: 'relative',
  },
  container: {
    height: '100%',
    width: '100%',
    backgroundColor: '#fff',
  },
  wrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
