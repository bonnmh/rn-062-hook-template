import {connect} from 'react-redux';
import ProfileDetailScreen from './ProfileDetailScreen';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileDetailScreen);
