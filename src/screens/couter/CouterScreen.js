import React, {useRef, useState} from 'react';
import {
  StatusBar,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useNavigation} from '@react-navigation/native';
import colors from '../../themes/Colors';
import {barStyle} from '../../themes/const';
import ApplicationStyle from '../../themes/Application.Style';

const CounterScreen = () => {
  const navigation = useNavigation();
  const counter = useRef(0);
  const [counterState, setCounterState] = useState(0);

  const updateState = () => {
    counter.current++;
    setCounterState(counterState + 1);
  };

  const renderToolbar = () => {
    return (
      <View style={styles.toolbar}>
        <StatusBar
          hidden={false}
          backgroundColor={colors.primary}
          barStyle={barStyle.lightContent}
        />
        <TouchableOpacity
          style={styles.viewWrapIcLeft}
          onPress={navigation.openDrawer}>
          <MaterialCommunityIcons
            name={'menu'}
            size={30}
            color={colors.white}
          />
        </TouchableOpacity>
        <View style={styles.viewWrapTitleToolbar}>
          <Text style={styles.titleToolbar}>Counter</Text>
        </View>
        <View style={styles.viewWrapIcRight} />
      </View>
    );
  };

  const renderButton = () => {
    return (
      <TouchableOpacity style={styles.btnIncrease} onPress={updateState}>
        <Text style={styles.textBtnIncrease}>Increase</Text>
      </TouchableOpacity>
    );
  };

  const renderData = () => {
    return (
      <View style={{margin: 20}}>
        <Text style={styles.text}>State count: {counterState}</Text>
        <Text style={styles.text}>Instance count: {counter.current}</Text>
      </View>
    );
  };

  return (
    <View style={styles.mainContainer}>
      {renderToolbar()}
      {renderButton()}
      {renderData()}
    </View>
  );
};
export default CounterScreen;
const styles = StyleSheet.create({
  ...ApplicationStyle,
  textContent: {
    color: colors.charcoalGrey,
    marginTop: 10,
    alignSelf: 'center',
  },
  btnIncrease: {
    backgroundColor: colors.charcoalGrey,
    width: 120,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    borderRadius: 5,
    alignSelf: 'center',
  },
  textBtnIncrease: {
    color: colors.white,
  },
  text: {
    color: colors.charcoalGrey,
  },
});
