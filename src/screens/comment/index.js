import {connect} from 'react-redux';
import CommentScreen from './CommentScreen';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CommentScreen);
