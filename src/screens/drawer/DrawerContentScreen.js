import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  StatusBar,
} from 'react-native';
import Animated from 'react-native-reanimated';
import {
  DrawerItemList,
  DrawerItem,
  DrawerContentScrollView,
} from '@react-navigation/drawer';

import Ionicons from 'react-native-vector-icons/Ionicons';
import colors from '../../themes/Colors';
import {isIphoneX} from '../../themes/Application.Style';
import {barStyle, fontSize} from '../../themes/const';
import IDs from '../IDs';

const DrawerContentScreen = ({progress, ...rest}) => {
  const renderToolbar = () => {
    return (
      <View>
        <TouchableOpacity
          style={styles.viewWrapIcLeft}
          onPress={() => rest.navigation.jumpTo(IDs.Profile)}>
          <Ionicons name="ios-home" color="white" size={30} />
        </TouchableOpacity>

        <View style={styles.viewWrapIcRight} />
      </View>
    );
  };

  return (
    <DrawerContentScrollView {...rest}>
      {renderToolbar()}
      <DrawerItemList {...rest} />
    </DrawerContentScrollView>
  );
};

export default DrawerContentScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  wrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    width: 200,
    height: 30,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
  },
  toolbar: {
    flexDirection: 'row',
    width: '100%',
    height: Platform.OS === 'android' ? 48 : isIphoneX() ? 88 : 78,
    paddingTop: Platform.OS === 'android' ? 0 : isIphoneX() ? 40 : 30,
    backgroundColor: colors.red,
    alignItems: 'center',
  },
  viewWrapTitleToolbar: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleToolbar: {
    color: colors.white,
    fontSize: fontSize.large,
  },
  viewWrapIcLeft: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 20,
  },
  icLeft: {
    width: 23,
    height: 23,
    tintColor: colors.white,
  },
  viewWrapIcRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icRight: {
    width: 23,
    height: 23,
    tintColor: colors.white,
  },
  textRight: {
    color: colors.white,
  },
  viewHorizontalLine: {
    height: 0.5,
    alignSelf: 'stretch',
  },
});
