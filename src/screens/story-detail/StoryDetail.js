/* eslint-disable react/self-closing-comp */
import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import Swiper from 'react-native-swiper';
import StoryDetailItem from '../../components/stories/StoryDetailItem/StoryDetailItem';
import {Logg} from '../../utils';
import Metrics from '../../themes/Metrics';
import CustomItemStory from '../../components/stories/StoryDetailItem/CustomItemStory';
import Stories from '../stories/Stories';

class StoryDetail extends Component {
  constructor(props) {
    super(props);
    this.swiper = {};
  }
  componentDidMount() {
    const {stories, setShowingStory} = this.props;
    const {position} = this.props?.route?.params;
    setShowingStory(stories[position], position);

    console.log('this.props', this.props);
  }

  onSwipeRightHandle() {
    console.log('right');
  }
  onSwipeLeftHandle() {
    console.log('Left');
  }
  onIndexChangedHandle = (index) => {
    const {setShowingStory} = this.props;
    setShowingStory({}, index);
  };
  render() {
    console.log('render parent', this.props, this.props.showingStory);
    const {stories, showingStory} = this.props;
    const {position} = this.props?.route?.params;

    return (
      <Swiper
        ref={(swiper) => (this.swiper = swiper)}
        onIndexChanged={this.onIndexChangedHandle}
        index={position}
        loop={false}
        showsPagination={false}>
        {stories?.map((story, index) => {
          return <Stories />;
        })}
      </Swiper>
    );
  }
}
{
  /* <StoryDetailItem
              swiper={this.swiper}
              showingStory={showingStory}
              position={index}
              key={story.id}
              storyDetail={story}
              stories={stories}
            />
            
            <CustomItemStory
              swiper={this.swiper}
              showingStory={showingStory}
              position={index}
              key={story.id}
              storyDetail={story}
              stories={stories}
            />*/
}
export default StoryDetail;
const styles = StyleSheet.create({
  backBtn: {
    padding: 10,
    paddingLeft: 0,
  },
  backgroundWrapper: {
    height: '100%',
    justifyContent: 'space-between',
  },
  reactionWrapper: {
    zIndex: 99,
    width: '100%',
    backgroundColor: 'rgba(0,0,0,0.0)',
    bottom: 10,
    paddingHorizontal: 20,
    marginRight: 50,
  },
  msgInput: {
    padding: 10,
    borderWidth: 2,
    borderColor: 'gray',
    borderRadius: 48,
    width: Metrics.appWidth * 0.6,
    color: '#fff',
    marginRight: 15,
  },
  iconWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  reactionIcon: {
    fontSize: 30,
    marginHorizontal: 5,
  },
  image: {
    height: Metrics.appHeight * 0.9,
  },
  userWrapper: {
    zIndex: 99,
    flexDirection: 'row',
    top: 40,
    width: '100%',
    backgroundColor: 'rgba(255, 255, 255, .0)',
    paddingHorizontal: 20,
  },
  userAvatar: {
    width: 40,
    height: 40,
    borderRadius: 50,
    marginRight: 15,
  },
  userInfoWrapper: {
    height: 40,
    justifyContent: 'center',
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
  },
  time: {
    color: '#fff',
  },
});
