import {connect} from 'react-redux';
import StoryDetail from './StoryDetail';
import {selectors, actions} from '../../store';

const mapStateToProps = (state) => {
  return {
    stories: selectors.home.getData(state),
    showingStory: selectors.home.getShowingStory(state),
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    setShowingStory: (story, position) =>
      dispatch(actions.home.setShowingStoryRequest({story, position})),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(StoryDetail);
