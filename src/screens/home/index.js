import {connect} from 'react-redux';
import HomeScreen from './HomeScreen';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = () => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
