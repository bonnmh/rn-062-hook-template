import React, {useEffect} from 'react';
import {
  StyleSheet,
  Dimensions,
  View,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {actions, selectors} from '../../store';
import {AppConst} from '../../constant';
import {PostTool} from '../../components';
import StoriesScreen from '../../components/stories';
import Test from '../../components/test/test';
import Stories from '../stories/Stories';
// import Line from './Line';
const HomeScreen = () => {
  const dispatch = useDispatch();
  const postData = useSelector((state) => selectors.post.getData(state));
  const isLoadingPost = useSelector((state) =>
    selectors.post.getLoadingPost(state),
  );
  const getStories = () => {
    dispatch(
      actions.home.fetchStoryRequest(
        AppConst.SelectPerferServices.fetchStories,
      ),
    );
  };
  const getPosts = () => {
    console.log('getPost');
    dispatch(
      actions.post.fetchPostRequest(AppConst.SelectPerferServices.fetchPost),
    );
    handleLogin();
  };
  const handleLogin = () => {
    dispatch(
      actions.auth.fetchLoginRequest({userName: 'bonnmh', password: '123456'}),
    );
  };

  // const getUsers = async () => {
  //   // http://0.0.0.0:3000/users
  //   try {
  //     const response = await Axios.get('http://0.0.0.0:3000/users');
  //     console.log(response);
  //   } catch (error) {
  //     console.error(error);
  //   }
  // };

  useEffect(() => {
    getPosts();
    getStories();
  }, []);
  if (postData.length === 0) return <View />;
  return (
    <SafeAreaView style={styles.wrap}>
      <Test />
      {/* <Line /> */}
      <ScrollView bounces={false} contentContainerStyle={styles.listContainter}>
        <PostTool />
        <StoriesScreen />
        <Stories />
        {/* <PostTool></PostTool>
        <Stories></Stories>
        {posts.map((item, index) => (
          <View key={index}>
            {index === 1 && <RecommendFriends></RecommendFriends>}
            <Item item={item} key={index}></Item>
          </View>
        ))} */}
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;

const screenHeight = Math.round(Dimensions.get('window').height);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  countTxt: {
    fontSize: 200,
    textAlign: 'center',
    lineHeight: screenHeight - 170,
    width: '100%',
    height: screenHeight - 170,
  },
  button: {
    width: '100%',
    height: 50,
    backgroundColor: 'black',
  },
  buttonText: {
    fontSize: 20,
    color: 'white',
    textAlign: 'center',
    lineHeight: 50,
  },
  listContainter: {
    flex: 1,
  },
  wrap: {
    flex: 1,
  },
});
