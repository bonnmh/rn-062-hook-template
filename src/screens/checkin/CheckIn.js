import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const CheckIn = () => {
  return (
    <View style={styles.wrap}>
      <Text>CheckIn</Text>
    </View>
  );
};

export default CheckIn;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
