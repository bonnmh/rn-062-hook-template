export const IDs = {
  Profile: 'profile@ProfileScreen',
  ProfileDetail: 'profile@ProfileDetailScreen',
  Follower: 'follow@FollowerScreen',
  FollowerDetail: 'follow@FollowerDetailScreen',
  Couter: 'couter@CouterScreen',
  DrawerContent: 'drawer@DrawerContentScreen',
  Home: 'Home',
  Comment: 'Comment',
  Group: 'Group',
  Watch: 'Watch',
  LiveStream: 'LiveStream',
  PhotoChooser: 'PhotoChooser',
  CheckIn: 'CheckIn',
  FullPostTool: 'FullPostTool',
  StoryDetail: 'StoryDetail',
};

export default IDs;
