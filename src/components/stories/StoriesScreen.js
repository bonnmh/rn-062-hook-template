import React from 'react';
import {View, ScrollView, StyleSheet} from 'react-native';
import Story from './Story';
import StoryAdder from './Story/StoryAdder';
const StoriesScreen = ({stories, user}) => {
  console.log('stories, user', stories, user);
  return (
    <View style={styles.container}>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        style={styles.stories}
        horizontal={true}>
        <StoryAdder user={user[0]} />
        {stories.map((story, index) => (
          <Story position={index} key={index} story={story} />
        ))}
      </ScrollView>
    </View>
  );
};
export default StoriesScreen;
const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    paddingHorizontal: 5,
    backgroundColor: '#fff',
    borderColor: '#ddd',
    borderWidth: 1,
    marginVertical: 10,
  },
  stories: {
    flexWrap: 'nowrap',
  },
});
