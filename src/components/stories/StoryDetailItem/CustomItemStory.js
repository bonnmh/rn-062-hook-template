import React, {useEffect} from 'react';
import {
  StyleSheet,
  ImageBackground,
  View,
  TouchableWithoutFeedback,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Logg} from '../../../utils';
import Test from '../../test/test';

const CustomItemStory = (props) => {
  useEffect(() => {
    Logg.info('props props', props);
  }, []);
  return (
    <View style={styles.wrap}>
      <ImageBackground
        blurRadius={50}
        imageStyle={{resizeMode: 'cover'}}
        style={styles.backgroundWrapper}
        source={{uri: props.storyDetail?.images[0]?.url}}>
        {props.storyDetail?.images.map((value, i) => {
          return (
            <View
              style={{
                height: '100%',
                width: '100%',
                justifyContent: 'center',
              }}>
              <Test />
              <Image
                style={{height: '40%', width: '100%'}}
                resizeMode="stretch"
                resizeMethod="scale"
                source={{uri: value.url}}
              />
            </View>
          );
        })}
        {/* <TouchableOpacity
          activeOpacity={1}
          onPress={() => alert(1)}
          style={{flex: 1}}
        />
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => alert(12)}
          style={{flex: 1}}
        /> */}
      </ImageBackground>
    </View>
  );
};

export default CustomItemStory;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },

  backgroundWrapper: {
    height: '100%',
    width: '100%',
    flexDirection: 'row',
  },
});
