import {connect} from 'react-redux';
import StoryDetailItem from './StoryDetailItem';
import {selectors} from '../../../store';

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(StoryDetailItem);
