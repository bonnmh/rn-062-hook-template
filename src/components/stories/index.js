import {connect} from 'react-redux';
import StoriesScreen from './StoriesScreen';
import {selectors} from '../../store';

const mapDispatchToProp = (dispatch) => {
  return {};
};
const mapStateToProps = (state) => ({
  stories: selectors.home.getData(state),
  user: selectors.auth.getData(state),
});
export default connect(mapStateToProps, mapDispatchToProp)(StoriesScreen);
