import React, {useState, useEffect} from 'react';
import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';
import FontAweSome5 from 'react-native-vector-icons/FontAwesome5';

import {navigation} from '../../app-navigation/rootNavigation';
import {selectors} from '../../store';
import IDs from '../../screens/IDs';

const GroupPostTool = (props) => {
  const user = useSelector((state) => selectors.auth.getData(state));
  const {isWriteToAnyOne, userX, isWriteToPage, page} = props;
  const [inputBgColor, setInputBgColor] = useState('#fff');
  const onLiveStreamPressHandler = () => {
    navigation.navigate(IDs.LiveStream);
  };
  const onPhotoUploaderPressHandler = () => {
    navigation.navigate(IDs.PhotoChooser);
  };
  const onCheckInPressHandler = () => {
    navigation.navigate('CheckIn');
  };
  const onFullPostToolPressHandler = () => {
    navigation.navigate('FullPostTool');
  };
  const onPressPostToAnyOneHandler = () => {
    navigation.navigate('FullPostTool', {
      isPostToAnyOne: true,
      userX: userX || page,
    });
  };

  const onPressSharePhotoToAnyOne = () => {
    navigation.navigate('PhotoChooser');
  };
  return (
    <View style={styles.container}>
      <View style={styles.postToolWrapper}>
        <TouchableOpacity activeOpacity={0.5} style={styles.userAvatarWrapper}>
          <Image
            source={{uri: user[0]?.avatar_url}}
            style={styles.userAvatar}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={
            isWriteToAnyOne
              ? onPressPostToAnyOneHandler
              : onFullPostToolPressHandler
          }
          style={styles.postInputWrapper}>
          <View
            style={{
              ...styles.postInput,
              backgroundColor: inputBgColor,
            }}>
            <Text>
              {isWriteToAnyOne || isWriteToPage
                ? `Write somethings to ${userX?.name || page?.name}`
                : 'What are you thinking ?'}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.postOptionsWrapper}>
        {!isWriteToAnyOne && !isWriteToPage && (
          <TouchableOpacity
            onPress={onLiveStreamPressHandler}
            activeOpacity={0.5}
            style={styles.postOptionItemWrapper}>
            <View style={styles.postOptionItem}>
              <FontAweSome5
                style={styles.postOptionIcon}
                name="video"
                color="red"
                size={16}
              />
              <Text>Live Stream</Text>
            </View>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          onPress={
            isWriteToAnyOne || isWriteToPage
              ? onPressPostToAnyOneHandler
              : onPhotoUploaderPressHandler
          }
          activeOpacity={0.5}
          style={styles.postOptionItemWrapper}>
          <View
            style={{...styles.postOptionItem, ...styles.postOptionItemMiddle}}>
            <FontAweSome5
              style={styles.postOptionIcon}
              name={isWriteToAnyOne || isWriteToPage ? 'edit' : 'image'}
              color="green"
              size={16}
            />
            <Text>
              {isWriteToAnyOne || isWriteToPage ? 'Write a post' : 'Photo'}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={
            isWriteToAnyOne || isWriteToPage
              ? onPressSharePhotoToAnyOne
              : onCheckInPressHandler
          }
          activeOpacity={0.5}
          style={styles.postOptionItemWrapper}>
          <View style={styles.postOptionItem}>
            <FontAweSome5
              style={styles.postOptionIcon}
              name={
                isWriteToAnyOne || isWriteToPage ? 'image' : 'map-marker-alt'
              }
              color="red"
              size={16}
            />
            <Text>
              {isWriteToAnyOne || isWriteToPage ? 'Share Photos' : 'Check in'}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default GroupPostTool;

const styles = StyleSheet.create({
  container: {
    borderTopColor: '#ddd',
    borderTopWidth: 1,
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
    marginTop: 10,
    backgroundColor: '#fff',
  },
  postToolWrapper: {
    padding: 10,
    flexDirection: 'row',
  },
  postOptionsWrapper: {
    flexDirection: 'row',
    height: 40,
    borderTopColor: '#ddd',
    borderTopWidth: 1,
    alignItems: 'center',
  },
  postOptionItemWrapper: {
    flex: 1,
    height: 40,
    justifyContent: 'center',
  },
  postOptionItem: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  postOptionItemMiddle: {
    borderRightColor: '#ddd',
    borderRightWidth: 1,
    borderLeftColor: '#ddd',
    borderLeftWidth: 1,
  },
  postOptionIcon: {
    marginRight: 5,
  },
  postInputWrapper: {
    borderRadius: 48,
    flex: 1,
    marginLeft: 5,
  },
  postInput: {
    justifyContent: 'center',
    borderRadius: 48,
    height: 40,
    width: '100%',
    borderColor: '#ddd',
    paddingHorizontal: 10,
    borderWidth: 1,
  },
  userAvatar: {
    width: 40,
    height: 40,
    borderRadius: 50,
  },
  userAvatarWrapper: {},
});
