import React, {useState, useEffect} from 'react';
import {Animated, Easing, Text, TouchableOpacity, View} from 'react-native';
import Metrics from '../../themes/Metrics';
const Test = () => {
  const [WIDTH, setWidth] = useState(new Animated.Value(0));

  useEffect(() => {}, []);

  return (
    <View style={styles.wrap}>
      <View style={{width: 300, backgroundColor: 'green'}}>
        <Animated.View style={[{width: WIDTH}, styles.bar]} />
      </View>

      <TouchableOpacity
        onPress={() => {
          Animated.timing(WIDTH, {
            toValue: 300,
            duration: 5000,
          }).start(() => {
            console.log('------->');
          });
        }}>
        <Text>ok</Text>
      </TouchableOpacity>
    </View>
  );
};
export default Test;
const styles = {
  bar: {
    height: 15,
    borderWidth: 1,
    borderColor: '#c72f06',
    backgroundColor: '#e75832',
    right: 0,
    zIndex: 10,
  },
  wrap: {
    width: '100%',
    alignItems: 'center',
    paddingVertical: 10,
    position: 'absolute',
    top: 40,
  },
};
