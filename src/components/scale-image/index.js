import React, {useEffect, useState} from 'react';
import {StyleSheet, Image, View} from 'react-native';

const ScaleImage = (props) => {
  const [source, setSource] = useState(props.source);
  const [_height, setHeight] = useState(0);
  const [_width, setWidth] = useState(0);
  useEffect(() => {
    Image.getSize(props.source, (width, height) => {
      if (props.width && !props.height) {
        setHeight(props.width);
        setWidth(height * (props.width / width));
      } else if (!props.width && props.height) {
        setHeight(width * (props.height / height));
        setWidth(props.height);
      } else {
        setHeight(height);
        setWidth(width);
      }
    });
  }, []);
  return (
    <Image
      source={source}
      style={{
        height: _height,
        width: _width,
        ...props.style,
      }}
    />
  );
};

export default ScaleImage;

const styles = StyleSheet.create({});
