export const colorsRandom = ['#f7eef7', '#f4f1f4', '#fbe9fb'];

export const appName = 'test';

export const api_url = 'http://0.0.0.0:3000/';

export const SelectPerferServices = {
  loginInput: 'users/sign-in',
  fetchPost: 'posts',
  fetchUser: 'users',
  fetchStories: 'stories?_expand=user',
};

export const ImageExtensions = {
  'image/jpeg': '.jpg',
  'image/png': '.png',
};
