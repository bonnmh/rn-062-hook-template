import {getStatusBarHeight} from 'react-native-status-bar-height';
import * as AppConst from './global';
export {getStatusBarHeight, AppConst};
