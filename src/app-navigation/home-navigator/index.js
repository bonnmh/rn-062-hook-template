/* eslint-disable react/react-in-jsx-scope */
import * as React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import HomeScreen from '../../screens/home/HomeScreen';
import CommentScreen from '../../screens/comment';

const Stack = createStackNavigator();
const HomeNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.ModalSlideFromBottomIOS,
        gestureResponseDistance: {vertical: 800},
      }}>
      <Stack.Screen name="Home" component={HomeScreen} />

      <Stack.Screen
        options={{cardStyle: {backgroundColor: 'transparent'}}}
        name="Comments"
        component={CommentScreen}
      />
    </Stack.Navigator>
  );
};
export default HomeNavigator;
