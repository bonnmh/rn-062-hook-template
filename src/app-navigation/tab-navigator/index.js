/* eslint-disable react/self-closing-comp */
import * as React from 'react';
import {getStatusBarHeight} from '../../constant';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';
import HomeNavigator from '../home-navigator';
import {createStackNavigator} from '@react-navigation/stack';
import GroupScreen from '../../screens/group';
import {useIsFocused} from '@react-navigation/native';
import WatchScreen from '../../screens/watch/WatchScreen';
import ProfileScreen from '../../screens/profile/ProfileScreen';
import ShortCutScreen from '../../screens/shortcut';
import colors from '../../themes/Colors';
import {NewTestProvider} from '../../screens/watch/context/useTestContext';
import GroupAnimation from '../../screens/group/group-animation/GroupAnimation';

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();
const STATUSBAR_HEIGHT = getStatusBarHeight(true);
const size = 20;
// const WatchScreenWithIsFocused = (props) => {
//   const isFocused = useIsFocused();
//   return <WatchScreen {...props} isFocused={isFocused} />;
// };
const watchTab = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Watch" component={WatchScreen} />
    </Stack.Navigator>
  );
};
const groupTab = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Group" component={GroupScreen} />
      <Stack.Screen name="Group" component={GroupAnimation} />
    </Stack.Navigator>
  );
};
const profileTab = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Profile" component={ProfileScreen} />
    </Stack.Navigator>
  );
};
const notificationTab = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Notification" component={notificationTab} />
    </Stack.Navigator>
  );
};
const shortCutTab = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="ShortCutIndex" component={ShortCutScreen} />
    </Stack.Navigator>
  );
};

const StackWithContext = (props) => (
  <NewTestProvider>
    <WatchScreen {...props} />
  </NewTestProvider>
);

StackWithContext.router = watchTab.router;

const MainTab = () => {
  const navigationOptions = {
    style: {
      paddingTop: STATUSBAR_HEIGHT,
    },
    showIcon: true,
    showLabel: false,
  };
  return (
    <Tab.Navigator initialRouteName="Group" tabBarOptions={navigationOptions}>
      <Tab.Screen
        options={{
          tabBarIcon: ({tintColor, focused}) => (
            <Icon
              name="home"
              size={size}
              color={focused ? colors.primary : '#ddd'}
            />
          ),
        }}
        name="Home"
        component={HomeNavigator}
      />
      <Tab.Screen
        options={{
          tabBarIcon: ({tintColor, focused}) => (
            <Icon
              name="users"
              size={size}
              color={focused ? '#318bfb' : '#ddd'}
            />
          ),
        }}
        name="Group"
        component={groupTab}
      />
      <Tab.Screen
        options={{
          tabBarIcon: ({tintColor, focused}) => (
            <Icon
              name="video"
              size={size}
              color={focused ? '#318bfb' : '#ddd'}
            />
          ),
        }}
        name="Watch"
        component={StackWithContext}
      />
      <Tab.Screen
        options={{
          tabBarIcon: ({tintColor, focused}) => (
            <Icon
              name="user-circle"
              size={size}
              color={focused ? '#318bfb' : '#ddd'}
            />
          ),
        }}
        name="Profile"
        component={profileTab}
      />
      {/* <Tab.Screen
        options={{
          tabBarIcon: ({tintColor, focused}) => (
            <Icon
              name="bell"
              size={size}
              color={focused ? '#318bfb' : '#ddd'}
            />
          ),
        }}
        name="Notification"
        component={notificationTab}
      /> */}
      {/* <Tab.Screen
        options={{
          tabBarIcon: ({tintColor, focused}) => (
            <Icon
              name="bars"
              size={size}
              color={focused ? '#318bfb' : '#ddd'}
            />
          ),
        }}
        name="ShortCut"
        component={shortCutTab}
      /> */}
    </Tab.Navigator>
  );
};

export default MainTab;
