import {StyleSheet} from 'react-native';
import colors from '../../Themes/Colors';
import ApplicationStyle from '../../themes/Application.Style';
// import {fontFamily, fontSize} from '../const';

export default StyleSheet.create({
  ...ApplicationStyle,
  textItemMenu: {
    // fontSize: fontSize.medium,
    // fontFamily: fontFamily.regular,
    color: colors.primary,
    marginLeft: 12,
  },
});
