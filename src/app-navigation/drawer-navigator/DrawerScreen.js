import React from 'react';
import {useWindowDimensions, StyleSheet} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';

import IDs from '../../screens/IDs';
import ProfileScreen from '../../screens/profile/ProfileScreen';
import FollowerScreen from '../../screens/follower/FollowerScreen';
import CouterScreen from '../../screens/couter/CouterScreen';
import DrawerContentScreen from '../../screens/drawer/DrawerContentScreen';

const Drawer = createDrawerNavigator();

const DrawerScreen = () => {
  const dimensions = useWindowDimensions();
  return (
    <Drawer.Navigator
      minSwipeDistance={30}
      keyboardDismissMode="none"
      statusBarAnimation="fade"
      hideStatusBar={true}
      drawerType={dimensions.width > 900 ? 'permanent' : 'front'}
      drawerStyle={[styles.drawerStyles, {width: dimensions.width * 0.7}]}
      drawerContent={(props) => <DrawerContentScreen {...props} />}>
      <Drawer.Screen
        name={IDs.Profile}
        component={ProfileScreen}
        options={{drawerLabel: 'Profile'}}
      />
      <Drawer.Screen
        name={IDs.Follower}
        component={FollowerScreen}
        options={{drawerLabel: 'Follower'}}
      />
      <Drawer.Screen
        name={IDs.Couter}
        component={CouterScreen}
        options={{drawerLabel: 'Couter'}}
      />
    </Drawer.Navigator>
  );
};

export default DrawerScreen;

const styles = StyleSheet.create({
  drawerStyles: {
    backgroundColor: '#c6cbef',
  },
});
