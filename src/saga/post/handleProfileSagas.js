import {call, put} from 'redux-saga/effects';
import {actions} from '../../store';
import {FetchHelper, Logg} from '../../utils';

function* handleFetchPost(action) {
  yield put(actions.post.isLoadingFetchPost(true));
  try {
    const data = yield call(FetchHelper.CallApi, {
      api: action.payload,
    });
    if (data) {
      yield put(actions.post.isLoadingFetchPost(false));
      yield put(actions.post.fetchPostSuccess(data));
    } else {
      Logg.info('call fail');
      yield put(actions.post.isLoadingFetchPost(false));
    }
  } catch (error) {
    Logg.info('error');
  }
}
export default handleFetchPost;
