import {takeLatest} from 'redux-saga/effects';
import {types} from '../../store';
import handleFetchPost from './handleProfileSagas';

export function* watchFetchPost() {
  yield takeLatest(types.post.FETCH_POST_REQUEST, handleFetchPost);
}
