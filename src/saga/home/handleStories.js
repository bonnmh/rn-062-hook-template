import {call, put} from 'redux-saga/effects';
import {actions} from '../../store';
import {FetchHelper} from '../../utils';

export default function* handleStories(action) {
  yield put(actions.home.isLoadingFetchStory(true));
  try {
    const data = yield call(FetchHelper.CallApi, {
      api: action.payload,
    });
    if (data) {
      yield put(actions.home.fetchStorySuccess(data));
    }
  } catch (error) {
  } finally {
    yield put(actions.home.isLoadingFetchStory(false));
  }
}
