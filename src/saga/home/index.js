import {takeLatest, all} from 'redux-saga/effects';
import {types} from '../../store';
import handleStories from './handleStories';

export function* watchHomeSaga() {
  yield all([takeLatest(types.home.FETCH_STORIES_REQUEST, handleStories)]);
}
