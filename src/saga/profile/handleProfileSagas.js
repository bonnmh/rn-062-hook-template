import {call, put} from 'redux-saga/effects';
import {getProfile} from '../../common/api';
import {actions} from '../../store';

function* handleGetProfile(action) {
  const response = yield call(getProfile, action.payload);
  yield put(actions.profile.isLoading(true));
  if (response.ok) {
    yield put(actions.profile.isLoading(false));
    yield put(actions.profile.getProfileSuccess(response.data));
  } else {
    if (
      response.problem !== 'NETWORK_ERROR' &&
      response.problem !== 'TIMEOUT_ERROR' &&
      response.problem !== 'CONNECTION_ERROR'
    ) {
      yield put(actions.profile.getProfileFail(response.problem));

      yield put(actions.profile.isLoading(false));
    } else {
      yield put(actions.profile.isLoading(false));
      yield put(actions.network.sendNetworkFail(response.problem));
      yield put(actions.profile.getProfileFail(response.problem));
    }
  }
}
export default handleGetProfile;
