import {takeLatest} from 'redux-saga/effects';
import handleGetProfile from './handleProfileSagas';
import {types} from '../../store';

export function* watchGetProfile() {
  yield takeLatest(types.profile.GET_PROFILE_REQUEST, handleGetProfile);
}
