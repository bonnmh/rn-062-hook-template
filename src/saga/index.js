import {all} from 'redux-saga/effects';
import {watchGetProfile} from './profile';
import {watchFetchPost} from './post';
import {watchFetchAuth} from './auth';
import {watchHomeSaga} from './home';
export default function* rootSaga() {
  yield all([
    watchGetProfile(),
    watchFetchPost(),
    watchFetchAuth(),
    watchHomeSaga(),
  ]);
}
