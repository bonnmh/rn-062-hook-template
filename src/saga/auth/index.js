import {takeLatest, all} from 'redux-saga/effects';
import {types} from '../../store';
import handleLogin from './handleLogin';
import handleFetchHighLightPhoto from './handleFetchHighLightPhoto';

export function* watchFetchAuth() {
  yield all([
    takeLatest(types.auth.FETCH_LOGIN_REQUEST, handleLogin),
    takeLatest(
      types.auth.FETCH_HIGH_LIGHT_PHOTO_REQUEST,
      handleFetchHighLightPhoto,
    ),
  ]);
}
