import {call, put} from 'redux-saga/effects';
import {actions} from '../../store';
import {FetchHelper, Logg} from '../../utils';

function* handleLogin(action) {
  yield put(actions.auth.isLoadingFetchLogin(true));
  try {
    const data = yield call(FetchHelper.CallApi, {
      api: `users?username=${action.payload.userName}&password=${action.payload.password}`,
    });
    if (data) {
      yield put(actions.auth.isLoadingFetchLogin(false));
      yield put(actions.auth.fetchLoginSuccess(data));
      yield put(actions.auth.isLoadingFetchLogin(true));
      const photo = yield call(FetchHelper.CallApi, {
        api: `users/${data[0].id}/photos?_limit=9&isHighLight=true`,
      });
      if (photo) {
        yield put(actions.auth.isLoadingFetchLogin(false));
        yield put(actions.auth.fetchHighLightPhotoSuccess(photo));
      }
    } else {
      Logg.info('call fail');
      yield put(actions.auth.isLoadingFetchLogin(false));
    }
  } catch (error) {
    Logg.info('error');
  }
}
export default handleLogin;
