import {put, call} from 'redux-saga/effects';
import {actions} from '../../store';
import {Logg, FetchHelper} from '../../utils';

function* handleFetchHighLightPhoto(action) {
  yield put(actions.auth.isLoadingFetchPhoto(true));
  try {
    const data = yield call(FetchHelper.CallApi, {
      api: `users/${action.payload}/photos?_limit=9&isHighLight=true`,
    });
    if (data) {
      yield put(actions.auth.fetchHighLightPhotoSuccess(data));
    }
  } catch (e) {
    Logg.error(e);
  } finally {
    yield put(actions.auth.isLoadingFetchPhoto(false));
  }
}
export default handleFetchHighLightPhoto;
