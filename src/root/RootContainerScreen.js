import React, {useEffect, useState} from 'react';
import styles from './RootContainerStyles';
import {Keyboard, Platform, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Toast from 'react-native-simple-toast';
import {useDispatch, useSelector} from 'react-redux';

import DrawerScreen from '../app-navigation/drawer-navigator/DrawerScreen';
import ProfileDetailScreen from '../screens/profile-detail/ProfileDetailScreen';
import FollowerDetailScreen from '../screens/follower-detail/FollowerDetailScreen';

import IDs from '../screens/IDs';
import {actions, selectors} from '../store';
import {navigationRef} from '../app-navigation/rootNavigation';
import MainTab from '../app-navigation/tab-navigator';
import livestream from '../screens/livestream';
import photoChooser from '../screens/photo-chooser';
import {FullPostTool} from '../screens/post-tool';
import checkin from '../screens/checkin';
import storyDetail from '../screens/story-detail';
import Video from '../components/permission/Video';

const Stack = createStackNavigator();

const RootContainerScreen = () => {
  const sendNetworkFail = useSelector((state) =>
    selectors.network.getErr(state),
  );
  const [isKeyboardShow, setIsKeyboardShow] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const dispatch = useDispatch();
  const clearNetworkStatus = () => dispatch(actions.network.clearNetworkFail);

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      (e) => {
        setIsKeyboardShow(true);
        setKeyboardHeight(e.endCoordinates.height);
      },
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setIsKeyboardShow(false);
      },
    );

    return () => {
      keyboardDidShowListener.remove();
      keyboardDidHideListener.remove();
    };
  }, []);

  if (sendNetworkFail) {
    switch (sendNetworkFail) {
      case 'NETWORK_ERROR':
        Toast.show('No network connection, please try again');
        break;
      case 'TIMEOUT_ERROR':
        Toast.show('Timeout, please try again');
        break;
      case 'CONNECTION_ERROR':
        Toast.show('DNS server not found, please try again');
        break;
      default:
        Toast.show(sendNetworkFail);
        break;
    }
    clearNetworkStatus();
  }

  return (
    <View style={styles.mainContainer}>
      <NavigationContainer
        onStateChange={(state) => console.log('New state is', state)}
        ref={navigationRef}>
        <Stack.Navigator initialRouteName="Drawer" headerMode={'none'}>
          <Stack.Screen name="Drawer" component={MainTab} />
          <Stack.Screen
            name={IDs.ProfileDetail}
            component={ProfileDetailScreen}
            options={{gestureEnabled: true, gestureDirection: 'horizontal'}}
          />
          <Stack.Screen
            options={{cardStyle: {backgroundColor: 'transparent'}}}
            name={IDs.StoryDetail}
            component={storyDetail}
          />
          <Stack.Screen name={IDs.LiveStream} component={livestream} />
          <Stack.Screen name={IDs.PhotoChooser} component={photoChooser} />
          <Stack.Screen
            options={{cardStyle: {backgroundColor: 'transparent'}}}
            name={IDs.FullPostTool}
            component={FullPostTool}
          />
          <Stack.Screen name={'Video'} component={Video} />

          <Stack.Screen name={IDs.CheckIn} component={checkin} />
          <Stack.Screen
            name={IDs.FollowerDetail}
            component={FollowerDetailScreen}
            options={{gestureEnabled: true, gestureDirection: 'horizontal'}}
          />
        </Stack.Navigator>
      </NavigationContainer>
      {/*Keyboard padding*/}
      {isKeyboardShow && Platform.OS === 'ios' ? (
        <View style={{height: keyboardHeight}} />
      ) : null}
    </View>
  );
};
export default RootContainerScreen;
