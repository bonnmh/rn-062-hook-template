const Types = {
  FETCH_STORIES_REQUEST: 'home@FETCH_STORIES_REQUEST',
  FETCH_STORIES_SUCCESS: 'home@FETCH_STORIES_SUCCESS',
  IS_LOADING_FETCH_STORIES: 'home@IS_LOADING_FETCH_STORIES',

  SHOWING_STORY_REQUEST: 'home@SHOWING_STORY_REQUEST',
};
const Actions = {
  fetchStoryRequest: (api) => ({
    type: Types.FETCH_STORIES_REQUEST,
    payload: api,
  }),
  fetchStorySuccess: (data) => ({
    type: Types.FETCH_STORIES_SUCCESS,
    payload: data,
  }),
  isLoadingFetchStory: (status) => ({
    type: Types.IS_LOADING_FETCH_STORIES,
    payload: status,
  }),
  setShowingStoryRequest: (data) => ({
    type: Types.SHOWING_STORY_REQUEST,
    payload: data,
  }),
};

export {Types, Actions};
