export const getData = (state) => state.home.data;
export const getLoadingStory = (state) => state.home.loadingStory;
export const getShowingStory = (state) => state.home.showingStory;
