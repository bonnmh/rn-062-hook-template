import {Types} from './actions';
import {Helper} from '../../utils';

const initialState = {
  data: [],
  loadingStory: false,
  showingStory: null,
};

export const reducer = Helper.createReducer(initialState, {
  [Types.FETCH_STORIES_SUCCESS]: ({state, action}) => {
    return {
      ...state,
      data: action.payload,
    };
  },
  [Types.IS_LOADING_FETCH_STORIES]: ({state, action}) => {
    return {
      ...state,
      loadingStory: action.payload,
    };
  },
  [Types.SHOWING_STORY_REQUEST]: ({state, action}) => {
    return {
      ...state,
      showingStory: {
        ...action.payload,
      },
    };
  },
});
export default reducer;
