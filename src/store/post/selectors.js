export const getData = (state) => state.post.data;
export const getLoadingPost = (state) => state.post.loadingPost;
export const getDataPostByID = (state) => state.post.dataPostByID;
export const getLoadingPostByID = (state) => state.post.loadingPostByID;
