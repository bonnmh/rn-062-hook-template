import {Types} from './actions';
import {Helper} from '../../utils';

const initialState = {
  data: [],
  loadingPost: false,
  dataPostByID: [],
  loadingPostByID: false,
};

export const reducer = Helper.createReducer(initialState, {
  [Types.FETCH_POST_SUCCESS]: ({state, action}) => {
    return {
      ...state,
      data: action.payload,
    };
  },
  [Types.IS_LOADING_FETCH_POST]: ({state, action}) => {
    return {
      ...state,
      loadingPost: action.payload,
    };
  },
  [Types.FETCH_POST_ID_SUCCESS]: ({state, action}) => {
    return {
      ...state,
      dataPostByID: action.payload,
    };
  },
  [Types.IS_LOADING_POST_ID]: ({state, action}) => {
    return {
      ...state,
      loadingPostByID: action.payload,
    };
  },
});
export default reducer;
