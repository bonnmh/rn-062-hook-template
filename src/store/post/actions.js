const Types = {
  FETCH_POST_REQUEST: 'post@FETCH_POST_REQUEST',
  FETCH_POST_SUCCESS: 'post@FETCH_POST_SUCCESS',
  IS_LOADING_FETCH_POST: 'post@IS_LOADING_FETCH_POST',

  FETCH_POST_ID_REQUEST: 'post@FETCH_POST_ID_REQUEST',
  FETCH_POST_ID_SUCCESS: 'post@FETCH_POST_ID_SUCCESS',
  IS_LOADING_POST_ID: 'post@IS_LOADING_POST_ID',
};
const Actions = {
  fetchPostRequest: (api) => ({type: Types.FETCH_POST_REQUEST, payload: api}),
  fetchPostSuccess: (data) => ({type: Types.FETCH_POST_SUCCESS, payload: data}),
  isLoadingFetchPost: (status) => ({
    type: Types.IS_LOADING_FETCH_POST,
    payload: status,
  }),

  fetchPostIDRequest: (api) => ({
    type: Types.FETCH_POST_ID_REQUEST,
    payload: api,
  }),
  fetchPostIDSuccess: (data) => ({
    type: Types.FETCH_POST_ID_SUCCESS,
    payload: data,
  }),
  isLoadingFetchPostID: (status) => ({
    type: Types.IS_LOADING_POST_ID,
    payload: status,
  }),
};

export {Types, Actions};
