import {Types as postTypes, Actions as postActions} from './actions';
import postReducer from './reducer';
import * as postSelectors from './selectors';

export {postTypes, postActions, postReducer, postSelectors};
