export const getFetching = (state) => state.profile.fetching;
export const getData = (state) => state.profile.data;
export const getErr = (state) => state.profile.err;
