const Types = {
  GET_PROFILE_REQUEST: 'GET_PROFILE_REQUEST',
  GET_PROFILE_SUCCESS: 'GET_PROFILE_SUCCESS',
  GET_PROFILE_FAIL: 'GET_PROFILE_FAIL',

  IS_LOADING: 'IS_LOADING',
};
const Actions = {
  getProfileRequest: (username) => ({
    type: Types.GET_PROFILE_REQUEST,
    payload: {username},
  }),
  getProfileSuccess: (data) => ({
    type: Types.GET_PROFILE_SUCCESS,
    payload: {data},
  }),
  getProfileFail: (err) => ({
    type: Types.GET_PROFILE_FAIL,
    payload: {err},
  }),
  isLoading: (value) => ({
    type: Types.IS_LOADING,
    payload: {value},
  }),
};

export {Types, Actions};
