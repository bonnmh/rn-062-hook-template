import {Types} from './actions';
import {Helper} from '../../utils';

const initialState = {fetching: false, data: null, err: null};

export const reducer = Helper.createReducer(initialState, {
  // [Types.GET_PROFILE_REQUEST]: ({state, action}) => {
  //   return {
  //     ...state,
  //     fetching: true,
  //     data: null,
  //     err: null,
  //   };
  // },
  [Types.GET_PROFILE_SUCCESS]: ({state, action}) => {
    return {
      ...state,
      fetching: false,
      data: action.payload.data,
      err: null,
    };
  },
  [Types.GET_PROFILE_FAIL]: ({state, action}) => {
    return {
      ...state,
      fetching: false,
      data: null,
      err: action.payload.err,
    };
  },
  [Types.IS_LOADING]: ({state, action}) => {
    return {
      ...state,
      fetching: action.payload.value,
    };
  },
});
export default reducer;
