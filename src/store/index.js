import {combineReducers} from 'redux';
import {
  profileReducer,
  profileTypes,
  profileActions,
  profileSelectors,
} from './profile';
import {
  networkReducer,
  networkTypes,
  networkActions,
  networkSelectors,
} from './network';
import {postActions, postReducer, postSelectors, postTypes} from './post';
import {authReducer, authSelectors, authActions, authTypes} from './auth';
import {homeReducer, homeTypes, homeActions, homeSelectors} from './home';

const rootReducer = combineReducers({
  profile: profileReducer,
  network: networkReducer,
  post: postReducer,
  auth: authReducer,
  home: homeReducer,
});

const types = {
  profile: profileTypes,
  network: networkTypes,
  post: postTypes,
  auth: authTypes,
  home: homeTypes,
};

const actions = {
  profile: profileActions,
  network: networkActions,
  post: postActions,
  auth: authActions,
  home: homeActions,
};
const selectors = {
  profile: profileSelectors,
  network: networkSelectors,
  post: postSelectors,
  auth: authSelectors,
  home: homeSelectors,
};

export {rootReducer, types, actions, selectors};
