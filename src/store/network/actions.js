const Types = {
  SEND_NETWORK_FAIL: 'network@SEND_NETWORK_FAIL',
  CLEAR_NETWORK_FAIL: 'network@CLEAR_NETWORK_FAIL',
};
const Actions = {
  sendNetworkFail: (err) => ({type: Types.SEND_NETWORK_FAIL, payload: {err}}),
  clearNetworkFail: () => ({type: Types.CLEAR_NETWORK_FAIL}),
};

export {Types, Actions};
