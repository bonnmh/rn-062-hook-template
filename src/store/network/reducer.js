import {Types} from './actions';
import {Helper} from '../../utils';

const initialState = {err: null};

export const reducer = Helper.createReducer(initialState, {
  [Types.SEND_NETWORK_FAIL]: ({state, action}) => {
    return {
      ...state,
      err: action.payload.err,
    };
  },
  [Types.CLEAR_NETWORK_FAIL]: ({state, action}) => {
    return {
      ...state,
      err: null,
    };
  },
});
export default reducer;
