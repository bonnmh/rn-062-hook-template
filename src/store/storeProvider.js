import createSagaMiddleware from 'redux-saga';
import {logger} from 'redux-logger';

import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-community/async-storage';
import {applyMiddleware, createStore} from 'redux';
import {persistReducer, persistStore} from 'redux-persist';
import {Provider} from 'react-redux';
import {rootReducer} from '../store/index';
import rootSaga from '../saga';

const sagaMiddleware = createSagaMiddleware();
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['auth', 'post'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(
  persistedReducer,
  applyMiddleware(sagaMiddleware, logger, thunk),
);
const persistor = persistStore(store);

const startSaga = () => sagaMiddleware.run(rootSaga);
export {Provider, startSaga, store, persistor};
