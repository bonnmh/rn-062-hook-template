const Types = {
  FETCH_LOGIN_REQUEST: 'auth@FETCH_LOGIN_REQUEST',
  FETCH_LOGIN_SUCCESS: 'auth@FETCH_LOGIN_SUCCESS',
  IS_LOADING_FETCH_LOGIN: 'auth@IS_LOADING_FETCH_LOGIN',
  FETCH_HIGH_LIGHT_PHOTO_REQUEST: 'auth@FETCH_HIGH_LIGHT_PHOTO_REQUEST',
  FETCH_HIGH_LIGHT_PHOTO_SUCCESS: 'auth@FETCH_HIGH_LIGHT_PHOTO_SUCCESS',
  IS_LOADING_FETCH_PHOTO: 'auth@IS_LOADING_FETCH_PHOTO',
};
const Actions = {
  fetchLoginRequest: (api) => ({type: Types.FETCH_LOGIN_REQUEST, payload: api}),
  fetchLoginSuccess: (data) => ({
    type: Types.FETCH_LOGIN_SUCCESS,
    payload: data,
  }),
  isLoadingFetchLogin: (status) => ({
    type: Types.IS_LOADING_FETCH_LOGIN,
    payload: status,
  }),
  fetchHighLightPhotoRequest: (userId) => ({
    type: Types.FETCH_HIGH_LIGHT_PHOTO_REQUEST,
    payload: userId,
  }),
  fetchHighLightPhotoSuccess: (data) => ({
    type: Types.FETCH_HIGH_LIGHT_PHOTO_SUCCESS,
    payload: data,
  }),
  isLoadingFetchPhoto: (status) => ({
    type: Types.IS_LOADING_FETCH_PHOTO,
    payload: status,
  }),
};

export {Types, Actions};
