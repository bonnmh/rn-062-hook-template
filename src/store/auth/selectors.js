export const getData = (state) => state.auth.data;
export const getLoadingLogin = (state) => state.auth.loadingLogin;
export const getPhotos = (state) => state.auth.photos;
export const getLoadingPhoto = (state) => state.auth.loadingPhoto;
