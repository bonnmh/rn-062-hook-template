import {Types} from './actions';
import {Helper} from '../../utils';

const initialState = {
  data: [],
  loadingLogin: false,
  photos: [],
  loadingPhoto: false,
};

export const reducer = Helper.createReducer(initialState, {
  [Types.FETCH_LOGIN_SUCCESS]: ({state, action}) => {
    return {
      ...state,
      data: action.payload,
    };
  },
  [Types.IS_LOADING_FETCH_LOGIN]: ({state, action}) => {
    return {
      ...state,
      loadingLogin: action.payload,
    };
  },
  [Types.FETCH_HIGH_LIGHT_PHOTO_SUCCESS]: ({state, action}) => {
    return {
      ...state,
      photos: action.payload,
    };
  },
  [Types.IS_LOADING_FETCH_PHOTO]: ({state, action}) => {
    return {
      ...state,
      loadingPhoto: action.payload,
    };
  },
});
export default reducer;
