import Logg from './Logg';
import {AppConst} from '../constant';
let accessToken = '';
export const Methods = {
  POST: 'POST',
  GET: 'GET',
  PUT: 'PUT',
};
let API_URL = AppConst.api_url;
export const setToken = (token) => {
  accessToken = 'bearer' + ' ' + token.accessToken;
};
export const CallApi = ({api, method = 'GET', body, fullapi}) => {
  return new Promise((resolve, reject) => {
    try {
      if (!api) {
        throw new Error('Missing endpoint.');
      }
      fetch(fullapi ? `${fullapi}` : `${API_URL}${api}`, {
        method,
        ...(body
          ? {
              body: JSON.stringify(body),
            }
          : {}),
        headers: {
          'Content-Type': 'application/json',
          Authorization: accessToken,
        },
      }).then((res) => {
        const {status} = res;
        if (status === 401 || status === 403) {
          Logg.info('lõi fetch api 401 ' + `${api}`);
        }
        res
          .json()
          .then((data) => {
            //console.log('xxxx11111'+ JSON.stringify(data) );
            if (data) {
              const {errorMessage, errorCode} = data;
              if (errorCode) {
                reject(new Error(`${errorMessage}`));
                return;
              }
            }
            resolve(data);
          })
          .catch(() => {
            if (status === 200) {
              resolve();
              return;
            }
            reject({message: 'Remote server fail with status ' + status});
          });
      });
    } catch (error) {
      reject(error);
    }
  });
};
