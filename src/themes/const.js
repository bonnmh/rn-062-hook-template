export const barStyle = {
  darkContent: 'dark-content',
  lightContent: 'light-content',
};

export const fontSize = {small: 14, medium: 16, large: 18};
