import {Dimensions, Platform, StatusBar} from 'react-native';

// Note: window mean application window size
const {width: appWidth, height: appHeight} = Dimensions.get('window');

const isAndroid = Platform.OS === 'android';
const isIOS = Platform.OS === 'ios';

const baseUnit = 8;
const getBaseUnitFactor = (multiplier: number = 1) => baseUnit * multiplier;

export default {
  isAndroid,
  isIOS,
  appHeight,
  appWidth,

  androidNavBarHeight: 56, // Fixed height from RNN
  statusBarHeight: isAndroid ? StatusBar.currentHeight : 20, //20 is hard core status bar height on iOS

  getBaseUnitFactor,
  pdMain: isAndroid ? 15 : 25,
};
